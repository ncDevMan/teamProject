<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Register - Aloha Music Hall</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/shop-item.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="indexl.php">Aloha Music Hall</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="viewUpcomingEvents.php">View Upcoming Events</a>
                </li>
                <li>
                    <a href="reserveSeating.php">Reserve Seating</a>
                </li>
                <li>
                    <a href="cancelReservation.php">Cancel Reservations</a>
                </li>
                <li>
                    <a href="changePersonalInformation.php">Change Personal Information</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!-- Page Content -->
<div class="container">
    <div class="row">
        <div class="col-md-3">
        </div>

        <div class="col-md-9 body-block">
            <div class="thumbnail">
                <img class="img-responsive" src="img/registerGuitar.jpg" alt="">
                <div class="caption-full main-form">
                    <?php



                    /*
                     * Customer Class
                     *
                     * @author: Nick
                     *
                     * This Class allows a Customer object to be created. We can use these records and submit them to a database.
                     *
                     */
                    class Customer{
                        function Customer($fn, $ln, $ad, $pn, $cc, $em, $pw, $pwc, $mem, $uid){
                            $firstName = $fn;
                            $lastName = $ln;
                            $address = $ad;
                            $phoneNumber = $pn;
                            $creditCard = $cc;
                            $email = $em;
                            $password = $pw;
                            $passwordConfirmation = $pwc;
                            $member = $mem;
                            $userID = $uid;
                            $db = '';

                            //database local
                            $user = 'alohaadmin';
                            $pass = 'admin';
                            $host = 'localhost';
                            $database = 'alohamusichall';

                            //test database connection
                            try{
                                $db = new PDO('mysql:host='.$host.';dbname='.$database.'', $user, $pass);
                            }catch(PDOException $e){
                                print 'CANNOT CONNECT!'. $e->getMessage() . "<br/>";
                                die();
                            }
                        }

                        /*
                         * registerCustomer() Function
                         *
                         * @Author: Nick
                         *
                         * This function will connect to the given database and insert the Customers
                         * information into a database.
                         *
                         * @return: Void
                         */
                        function registerCustomer(){
                            global $firstName;
                            global $lastName;
                            global $address;
                            global $phoneNumber;
                            global $creditCard;;
                            global $email;
                            global $password;
                            global $passwordConfirmation;
                            global $member;
                            global $userID;

                            global $db;


                            $user = 'alohaadmin';
                            $pass = 'admin';
                            $host = 'localhost';
                            $database = 'alohamusichall';

                            $db = new PDO('mysql:host='.$host.';dbname='.$database.'', $user, $pass);

                            try{
                                $sql = "INSERT INTO customers (firstName, lastName, address, phoneNumber, creditCard, email, userID, password, isMember) VALUES (:firstName, :lastName, :address,
                                 :phoneNumber, :creditCard, :email, :userID, :password, :member)";
                                $q = $db->prepare($sql);
                                $q->execute(array(
                                    ':firstName'=>$firstName,
                                    ':lastName'=>$lastName,
                                    ':address'=>$address,
                                    ':phoneNumber'=>$phoneNumber,
                                    ':creditCard'=>$creditCard,
                                    ':email'=>$email,
                                    ':userID'=> $userID,
                                    ':password'=> $password,
                                    ':member'=>$member
                                ));
                            }catch(PDOException $e){
                                print 'CANNOT CONNECT!'. $e->getMessage() . "<br/>";
                                die();
                            }
                        }

                        /*
                         * closeDatabase() function
                         *
                         * @author: Nick
                         *
                         * This function will close the database after the record has been created.
                         *
                         * @return: void
                         */
                        function closeDatabase(){
                            global $db;

                            $db = null;
                        }

                    }

                    /*
                     * Main*************************************************************
                     */

                    /*
                     * generateID() Function
                     *
                     * @author: Nick
                     *
                     * This function is used to create a USERID when the user registers for membership or customership
                     *
                     *@return: Integer (ID)
                     *
                     */
                    function generateID($lastName){
                        $number = rand(1,1000);

                        $id = $lastName.$number;

                        return $id;
                    }

                    /*
                     * passwordError() Function
                     *
                     * @author: Nick
                     *
                     * This Function will detect that the passwords do not match
                     * then prompt the user about the error.
                     *
                     * @return: void
                     */
                    function passwordError(){
                        echo "<h2>Error</h2>";
                        echo "<br/>";
                        echo "<p>";

                        $link_address = 'registerMember.php';
                        print "<h3>Your passwords did not match!!</h3>";
                        echo "<br/>";
                        print "Please go " ."<a href='$link_address'><button>Back</button></a>". " and try again.";
                    }

                    /**
                     * processing page to grab all the data from the "Register New Customer" form
                     */
                    //Grab all the data from the forms
                    $firstName = $_POST['firstname'];
                    $lastName = $_POST['lastname'];
                    $address = $_POST['address'];
                    $phoneNumber = $_POST['phonenumber'];
                    $creditCard = $_POST['creditcard'];
                    $email = $_POST['email'];
                    $password = $_POST['password'];
                    $passwordConfirmation = $_POST['passwordconfirm'];

                    //This is Checking to see if the Member boxed is selected.
                    //If this member box is selected, lets just set it equal to one.
                    $status = '';
                    if (isset($_POST['member']) && $_POST['member'] == 'Member'){
                        $member = 1;
                        $status = "Member";
                    }
                    //if the checkbox is not selected- set it equal to 0
                    else{
                        $member = 0;
                        $status = 'Customer';
                    }


                    if ($password != $passwordConfirmation){
                        passwordError();
                    }
                    else{

                        $userID = generateID($lastName);

                        //Create a new Customer object
                        $Customer = new Customer($firstName, $lastName, $address, $phoneNumber, $creditCard, $email, $password, $passwordConfirmation, $member, $userID);
                        $Customer->registerCustomer();
                        $Customer->closeDatabase();

                        //Display---------------------------------------------------------------------------
                        echo "<h2>Congratulations!</h2>";
                        echo "<br/>";
                        echo "<p>";
                        echo "<h3>".$firstName .' ' .$lastName."</h3>";
                        echo"</br>";
                        echo "You have successfully registered as a "."<strong>".$status. "</strong>"." at Aloha Music Hall!";
                        echo"</br>";
                        echo"</br>";
                        echo "Your USERID is "."<strong>".$userID."</strong>"."."." Please save this ID and your password, as it is your login credentials.";
                    }

?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container container-footer">
    <!-- Footer -->
    <footer>
        <div class="row">
            <div class="col-lg-12 footer">
                <p><strong>&copy; Aloha Music Hall. LLC</strong></p
            </div>
        </div>
    </footer>

</div>
<!-- /.container -->

<!-- jQuery -->
<script src="js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

</body>

</html>


