-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 08, 2015 at 07:50 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `alohamusichall`
--

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phoneNumber` varchar(255) NOT NULL,
  `creditCard` int(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `userID` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `isMember` int(1) NOT NULL,
  `reservationsTotal` int(1) DEFAULT '0',
  `confirmNumber` int(64) DEFAULT NULL,
  PRIMARY KEY (`userID`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`firstName`, `lastName`, `address`, `phoneNumber`, `creditCard`, `email`, `userID`, `password`, `isMember`, `reservationsTotal`, `confirmNumber`) VALUES
('Nick', 'Conn', '3534 eku lane', '857-537-5344', 534534534, 'n@gmail.com', 'Conn842', 'qwerty', 1, 0, NULL),
('albert', 'hall', '56 maple lane', '606-854-5432', 4564564, 'ahall@gmail.com', 'hall370', 'qwerty', 1, 0, NULL),
('Scotty', 'Jones', '5354', '54645643453', 35345345, 'Jerry@gmail.com', 'Jones395', 'qwerty', 1, 2, NULL),
('Frankie', 'Martin', '5534', '6456559556', 43345345, 'frank@gmail.com', 'Martin328', 'loggy', 1, 1, NULL),
('Michael', 'Scott', '56 Shady Bvld', '859-878-2598', 2589, 'michael-scott@dmf.com', 'Scott647', 'qwerty', 1, 0, NULL),
('Nicole', 'Short', '89 Shady Lane', '859-784-6538', 2147483647, 'nicoles@hotmail.com', 'Short908', 'qwerty', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE IF NOT EXISTS `events` (
  `id` int(6) NOT NULL,
  `name` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `totalReserved` int(3) NOT NULL DEFAULT '0',
  `vipReserved` int(35) NOT NULL DEFAULT '0',
  `maxCapacity` int(3) NOT NULL DEFAULT '140',
  `price` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `name`, `time`, `date`, `totalReserved`, `vipReserved`, `maxCapacity`, `price`) VALUES
(1, 'Madison County Band', '8pm - 10pm', 'May 8th, 2015', 34, 0, 104, 13.99),
(2, 'New York Orchestra', '8pm - 10pm', 'May 8th, 2015', 2, 0, 104, 28.99),
(3, 'String Quart', '8pm - 10pm', 'May 16th, 2015', 87, 0, 104, 28.99),
(4, 'Brash Smash!', '3pm - 10pm', 'May 18th, 2015', 94, 0, 104, 34.99),
(5, 'Winds of the North- Woodwind Quintet', '9pm - 11pm', 'May 21st, 2015', 2, 0, 104, 78.99);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
