<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Aloha Music Hall</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/shop-item.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header title">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Aloha Music Hall</a>
        </div>
        <div class="collapse navbar-collapse toolbar" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="login.php">Login</a>
                </li>
                <li>
                    <a href="registerMember.php">Register New Customer</a>
                </li>
                <li>
                    <a href="viewUpcomingEventsu.php">View Upcoming Events</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<!-- Page Content -->
<div class="container">

    <div class="row">

        <div class="col-md-3">
        </div>

        <div class="col-md-9 index-col">

            <div class="thumbnail">
                <div class="caption-full main-form body-block">
                    <h2>Aloha Music Hall</h2>
                    <p><strong>Aloha is a music theater in Lexington, Kentucky.
                    We are home to where some of the most famous musicians got their start.
                    Found in 1956, We are one of the oldest theaters going!
                    </p>
                    <p>Don't let this fool you, we have done many renovations to this wonder venue!</p>
                    <p>Come view our upcoming events! If anything excites you, like we all know music does, register to become a VIP or a regular customer! The VIP members get
                    a great personal discount. Come join to find out more!</p>
                    </strong>
                    </br>
                    <img class="img-responsive" src="img/homepageStage.jpg" alt="">
                </div>
            </div>

        </div>

    </div>

</div>
<!-- /.container -->

<div class="container container-footer index-footer">
    <footer>
        <div class="row">
            <div class="col-lg-12 footer index-foot-text">
                <p><strong>&copy; Aloha Music Hall. LLC</strong></p>
            </div>
        </div>
    </footer>

</div>
<!-- /.container -->

<!-- jQuery -->
<script src="js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

</body>

</html>
