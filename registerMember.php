<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Register - Aloha Music Hall</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/shop-item.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">Aloha Music Hall</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse toolbar" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="login.php">Login</a>
                </li>
                <li>
                    <a href="registerMember.php">Register New Customer</a>
                </li>
                <li>
                    <a href="viewUpcomingEventsu.php">View Upcoming Events</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!-- Page Content -->
<div class="container">
    <div class="row">
        <div class="col-md-3">
        </div>

        <div class="col-md-9 body-block">
            <div class="thumbnail">
                <img class="img-responsive" src="img/registerGuitar.jpg" alt="">
                <div class="caption-full main-form">
                    <h2>Register a Customer</h2>
                    <p>
                    <form name="form" action="processMember.php" method="post"><strong>
                            First Name:<br/>
                            <input type="text" name="firstname">
                        </strong>
                    <br/>
                        <br/>
                    <strong>
                            Last Name:<br/>
                            <input type="text" name="lastname">
                        </strong>
                    <br/>
                        <br/>
                    <strong>
                            Address:<br/>
                            <input type="text" name="address">
                        </strong>
                    <br/>
                    <br/>
                    <strong>
                            Phone Number:<br/>
                            <input type="text" name="phonenumber">
                        </strong>
                    <br/>
                        <br/>
                    <strong>
                            Credit Card:<br/>
                            <input type="text" name="creditcard">
                        </strong>
                    <br/>
                        <br/>
                    <strong>
                            Email:<br/>
                            <input type="text" name="email">
                        </strong>
                    <br/>
                        <br/>
                    <strong>
                            Login Password:<br/>
                            <input type="password" name="password">
                        </strong>
                    <br/>
                        <br/>
                    <strong>
                            Login Password Confirmation:<br/>
                            <input type="password" name="passwordconfirm">
                        </strong>
                    <br/>
                        <br/>
                        <strong>Want to be a member?</strong>
                        <br/>
                        <strong>This will entitle you to 10% of all seat prices!</strong>
                        <br/>
                        <input type="checkbox" name="member" value="Member" name="isMember">
                        <br/>
                        <br/>
                        <input type="submit" onclick="return validateForm()"/>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="container container-footer">
    <!-- Footer -->
    <footer>
        <div class="row">
            <div class="col-lg-12 footer">
                <p><strong>&copy; Aloha Music Hall. LLC</strong></p
            </div>
        </div>
    </footer>

</div>
<!-- /.container -->

<!-- jQuery -->
<script src="js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

<script src="js/validation.js"></script>

</body>

</html>
