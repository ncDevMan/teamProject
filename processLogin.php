<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Login</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/shop-item.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">Aloha Music Hall</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse toolbar" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="index.php">Logout</a>
                </li>
                <li>
                    <a href="viewUpcomingEvents.php">View Upcoming Events</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!-- Page Content -->
<div class="container">
    <div class="row">
        <div class="col-md-3">
        </div>

        <div class="col-md-9 login-body-block body-block">
            <div class="thumbnail">
                <div class="caption-full main-form">
                    <?php


                    class Customer{
                        /*
                         * Customer Constructor
                         *
                         * @author: Nick
                         */
                        function Customer(){
                            global $db;

                            //database local
                            $user = 'alohaadmin';
                            $pass = 'admin';
                            $host = 'localhost';
                            $database = 'alohamusichall';

                            //test database connection
                            try{
                                $db = new PDO('mysql:host='.$host.';dbname='.$database.'', $user, $pass);
                            }catch(PDOException $e){
                                print 'CANNOT CONNECT!'. $e->getMessage() . "<br/>";
                                die();
                            }
                        }


                        /*
                         * checkLogin() function
                         *
                         * @author: Nick
                         *
                         * This function will check the login credentials for the system and will output a page accordingly.
                         *
                         * @return: void
                         */
                        function checkLogin($userID, $password){

                            global $db;

                            $stmt = ("SELECT userid, password FROM customers WHERE userID = :userid AND password = :password");
                            $sql = $db -> prepare ($stmt);
                            $sql-> execute(array(':userid'=>$userID, ':password' => $password));
                            $results = $sql->fetch(PDO::FETCH_ASSOC);

                            //If the user name and password does not exist
                            if(!$results){
                                echo "<h2>Error</h2>";
                                echo "<br/>";
                                echo "<p>";

                                $link_address = 'login.php';
                                print "<h3>You have invalid login credentials!</h3>";
                                echo "<br/>";
                                print "Please go back" ."<a href='$link_address'><button>Back</button></a>". " and try again.";
                            }

                            //The user name and password exists!!!!!
                            else{
                                //Track who is logged in
//                                session_start();
//                                $_SESSION["userID"] = $userID;
                                //grab reservations
                                $stmt = ("SELECT reservationsTotal FROM customers WHERE userID = :userid AND password = :password");
                                $sql = $db -> prepare ($stmt);
                                $sql-> execute(array(':userid'=>$userID, ':password' => $password));
                                $resevationResults = $sql->fetch(PDO::FETCH_NUM);

//                                $stmt = ("SELECT firstName, lastName FROM customers WHERE userID = :userid AND password = :password");
//                                $sql = $db -> prepare ($stmt);
//                                $sql-> execute(array(':userid'=>$userID, ':password' => $password));
//                                $nameResults = $sql->fetchAll();


                                $reservations = $resevationResults[0];
                                echo "<h2>Welcome ".$userID."!</h2>";
                                echo "You are now logged in.";
                                echo "You have a total of <strong>".$reservations."</strong> reservations already.";
                                echo "<br/>";
                                echo "<br/>";
                                echo "<br/>";
                                $link_address = 'viewUpcomingEvents.php';
                                $reservationsLeft = 4 - $reservations;
                                print "<h3>You have $reservationsLeft reservations left!</h3>";



                            }

                        }
                    }


                    $userID = $_POST['userid'];
                    $password = $_POST['password'];

//                    Main---------------------------------------------------------------------------------------------

                    $Customer = new Customer();
                    $Customer->checkLogin($userID, $password);


                    ?>
                </div>
            </div>

        </div>

    </div>

</div>

<div class="container container-footer process-login-footer">
    <!-- Footer -->
    <footer>
        <div class="row">
            <div class="col-lg-12 footer">
                <p><strong>&copy; Aloha Music Hall. LLC</strong></p>
            </div>
        </div>
    </footer>

</div>
<!-- /.container -->

<!-- jQuery -->
<script src="js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

</body>

</html>
