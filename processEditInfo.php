<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Edit Personal Information</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/shop-item.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="indexl.php">Aloha Music Hall</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse toolbar" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="index.php">Logout</a>
                </li>
                <li>
                    <a href="viewUpcomingEvents.php">View Upcoming Events</a>
                </li>
                <li>
                    <a href="reserveSeating.php">Reserve Seating</a>
                </li>
                <li>
                    <a href="cancelReservation.php">Cancel Reservations</a>
                </li>
                <li>
                    <a href="changePersonalInformation.php">Change Personal Information</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!-- Page Content -->
<div class="container">
<div class="row">
<div class="col-md-3">
</div>

<div class="col-md-9 body-block">
<div class="thumbnail">
<img class="img-responsive" src="img/processEdit-saxophone.jpg" alt="">
<div class="caption-full main-form">
<?php



/*
 * Customer Class
 *
 * @author: Nick
 *
 * This Class allows a Customer object to be created. We can use these records and submit them to a database.
 *
 */
class Customer{
    function Customer($fn, $ln, $ad, $pn, $cc, $em, $pw, $pwc, $uid, $mem){
        $firstName = $fn;
        $lastName = $ln;
        $address = $ad;
        $phoneNumber = $pn;
        $creditCard = $cc;
        $email = $em;
        $password = $pw;
        $passwordConfirmation = $pwc;
        $userID = $uid;
        $db = '';
        $member = $mem;

        //database local
        $user = 'alohaadmin';
        $pass = 'admin';
        $host = 'localhost';
        $database = 'alohamusichall';

        //test database connection
        try{
            $db = new PDO('mysql:host='.$host.';dbname='.$database.'', $user, $pass);
        }catch(PDOException $e){
            print 'CANNOT CONNECT!'. $e->getMessage() . "<br/>";
            die();
        }
    }

    /*
     * registerCustomer() Function
     *
     * @Author: Nick
     *
     * This function will connect to the given database and insert the Customers
     * information into a database.
     *
     * @return: Void
     */
    function registerCustomer(){
        global $firstName;
        global $lastName;
        global $address;
        global $phoneNumber;
        global $creditCard;;
        global $email;
        global $password;
        global $passwordConfirmation;
        global $member;
        global $userID;

        global $db;


        $user = 'alohaadmin';
        $pass = 'admin';
        $host = 'localhost';
        $database = 'alohamusichall';

        $db = new PDO('mysql:host='.$host.';dbname='.$database.'', $user, $pass);

        try{
            $sql = "INSERT INTO customers (firstName, lastName, address, phoneNumber, creditCard, email, userID, password, isMember) VALUES (:firstName, :lastName, :address,
                                 :phoneNumber, :creditCard, :email, :userID, :password, :member)";
            $q = $db->prepare($sql);
            $q->execute(array(
                ':firstName'=>$firstName,
                ':lastName'=>$lastName,
                ':address'=>$address,
                ':phoneNumber'=>$phoneNumber,
                ':creditCard'=>$creditCard,
                ':email'=>$email,
                ':userID'=> $userID,
                ':password'=> $password,
                ':member'=>$member
            ));
        }catch(PDOException $e){
            print 'CANNOT CONNECT!'. $e->getMessage() . "<br/>";
            die();
        }
    }

    /*
     * closeDatabase() function
     *
     * @author: Nick
     *
     * This function will close the database after the record has been created.
     *
     * @return: void
     */
    function closeDatabase(){
        global $db;

        $db = null;
    }


    /*has_presence() function
    * @author: Nick
     *
     * This function is used to check if the Customer wants to date the said field.
     * It will check to see if it is null or blank
     *
     * @return: Boolean
     *
     */
    function has_presence($value)
    {
        return isset($value) && $value !=="";
    }

    function editCustomer(){
        global $firstName;
        global $lastName;
        global $address;
        global $phoneNumber;
        global $creditCard;;
        global $email;
        global $password;
        global $passwordConfirmation;
        global $member;
        global $userID;

        global $db;


        $user = 'alohaadmin';
        $pass = 'admin';
        $host = 'localhost';
        $database = 'alohamusichall';

        $db = new PDO('mysql:host='.$host.';dbname='.$database.'', $user, $pass);

        try{
            //UPDATE FIRST NAME
            if($this->has_presence($firstName)){
                    $sql = "UPDATE customers SET firstName = :firstName WHERE userID = :userID";
                    $q = $db->prepare($sql);
                    $q->execute(array(
                        ':firstName'=>$firstName,
                        ':userID'=> $userID,
                    ));
            }

            //UPDATE LAST NAME
            if($this->has_presence($lastName)){
                
                $sql = "UPDATE customers SET lastName = :lastName WHERE userID = :userID";
                $q = $db->prepare($sql);
                $q->execute(array(
                    ':lastName'=>$lastName,
                    ':userID'=> $userID,
                ));
            }

            //UPDATE address
            if($this->has_presence($address)){
                
                $sql = "UPDATE customers SET address = :address WHERE userID = :userID";
                $q = $db->prepare($sql);
                $q->execute(array(
                    ':address'=>$address,
                    ':userID'=> $userID,
                ));
            }

            //UPDATE phoneNumber
            if($this->has_presence($phoneNumber)){
                
                $sql = "UPDATE customers SET phoneNumber = :phoneNumber WHERE userID = :userID";
                $q = $db->prepare($sql);
                $q->execute(array(
                    ':phoneNumber'=>$phoneNumber,
                    ':userID'=> $userID,
                ));
            }

            //UPDATE creditCard
            if($this->has_presence($creditCard)){
                
                $sql = "UPDATE customers SET creditCard = :creditCard WHERE userID = :userID";
                $q = $db->prepare($sql);
                $q->execute(array(
                    ':creditCard'=>$creditCard,
                    ':userID'=> $userID,
                ));
            }

            //UPDATE email
            if($this->has_presence($email)){
                
                $sql = "UPDATE customers SET email = :email WHERE userID = :userID";
                $q = $db->prepare($sql);
                $q->execute(array(
                    ':email'=>$email,
                    ':userID'=> $userID,
                ));
            }

            //UPDATE password
            if($this->has_presence($password)){
                
                $sql = "UPDATE customers SET password = :password WHERE userID = :userID";
                $q = $db->prepare($sql);
                $q->execute(array(
                    ':password'=>$password,
                    ':userID'=> $userID,
                ));
            }

            if ($member == 1){
                $sql = "UPDATE customers SET isMember = :member WHERE userID = :userID";
                $q = $db->prepare($sql);
                $q->execute(array(
                    ':member'=> $member,
                    ':userID' => $userID
                ));
            }



        }catch(PDOException $e){
            print 'CANNOT CONNECT!'. $e->getMessage() . "<br/>";
            die();
        }

        $this->displayInformation($userID);

    }
    /*
     * displayInformation() Function
     * @author: Nick
     * @parameters: USERid
     * This funciton will return all of the users information from the database
     *
     * @return: void
     */
    function displayInformation($userID){
        global $db;

        $stmt = $db -> prepare("SELECT * from customers WHERE userID = :userID");
        $stmt -> execute(array(':userID' => $userID));

        $results = $stmt -> fetchAll();

        
        $firstName = $results['0']['firstName'];
        $lastName = $results['0']['lastName'];
        $address = $results['0']['address'];
        $phoneNumber = $results['0']['phoneNumber'];
        $creditCard = $results['0']['creditCard'];
        $email = $results['0']['email'];
        $reservationsTotal = $results['0']['reservationsTotal'];
        $member = $results['0']['isMember'];

        //Display---------------------------------------------------------------------------
        echo "<h2>Congratulations!</h2>";
        echo "<br/>";
        echo "<p>";
        echo "<h3>".$firstName .' ' .$lastName."</h3>";

        echo "Here is all of your information- including the newly updated.";
        echo "<br/>";
        echo "<h4>Address: ".$address."</h4>";
        echo "<br/>";
        echo "<h4>Phone Number: ".$phoneNumber."</h4>";
        echo "<br/>";
        echo "<h4>Credit Card: ".$creditCard."</h4>";
        echo "<br/>";
        echo "<h4>Email: ".$email."</h4>";
        echo "<br/>";
        echo "<br/>";
        echo "<h4>You have a total of: <strong>".$reservationsTotal." </strong> reservation(s) currently. </h4>";
        echo "<br/>";
        echo "<br/>";
        if ($member = 1){
            echo "<h4>You are a <h2>Member!</h2> <h4> You will save 10% on all ticket prices!</h4>";
        }
        else{
            echo "<h4>You are a <h2>Customer!</h2> <h4> You will <strong>NOT</strong> save 10% on all ticket prices! Edit your Personal Information and become a member today!</h4>";

        }

    }
}

/*
 * Main*************************************************************
 */


/*
 * passwordError() Function
 *
 * @author: Nick
 *
 * This Function will detect that the userID is not in the database
 * then prompt the user about the error.
 *
 * @return: void
 */
function usernameError($userID){
    echo "<h2>Error</h2>";
    echo "<br/>";
    echo "<p>";

    $link_address = 'changePersonalInformation.php';
    print "<h3>Your UserID,<strong>".$userID ." </strong> was invalid!!</h3>";
    echo "<br/>";
    print "Please go " ."<a href='$link_address'><button>Back</button></a>". " and try again.";

}

/*
 * passwordError() Function
 *
 * @author: Nick
 *
 * This Function will detect that the passwords do not match
 * then prompt the user about the error.
 *
 * @return: void
 */
function passwordError(){
    echo "<h2>Error</h2>";
    echo "<br/>";
    echo "<p>";

    $link_address = 'changePersonalInformation.php';
    print "<h3>Your passwords did not match!!</h3>";
    echo "<br/>";
    print "Please go " ."<a href='$link_address'><button>Back</button></a>". " and try again.";
}

/**
 * processing page to grab all the data from the "Register New Customer" form
 */
//Grab all the data from the forms
$firstName = $_POST['firstname'];
$lastName = $_POST['lastname'];
$address = $_POST['address'];
$phoneNumber = $_POST['phonenumber'];
$creditCard = $_POST['creditcard'];
$email = $_POST['email'];
$password = $_POST['password'];
$passwordConfirmation = $_POST['passwordconfirm'];
$userID = $_POST['userID'];

$status = '';
if (isset($_POST['member']) && $_POST['member'] == 'Member'){
    $member = 1;
    $status = "Member";
}
//if the checkbox is not selected- set it equal to 0
else{
    $member = 0;
    $status = 'Customer';
}


//Password check first thing
if ($password != $passwordConfirmation){
    passwordError();
}


$user = 'alohaadmin';
$pass = 'admin';
$host = 'localhost';
$database = 'alohamusichall';
//Connect to the database to check
try{
    $db = new PDO('mysql:host='.$host.';dbname='.$database.'', $user, $pass);
}catch(PDOException $e){
    print 'CANNOT CONNECT!'. $e->getMessage() . "<br/>";
    die();
}


$stmt = $db -> prepare("SELECT userID from customers WHERE userID = :userID");
$stmt -> execute(array(':userID' => $userID));
$checkUser = $stmt->fetch(PDO::FETCH_ASSOC);

if(!$checkUser){
    usernameError($userID);
}


if (empty($firstName)){
$firstName = '';
}
if (empty($lastName)){
$lastName = '';
}
if (empty($address)){
$phoneNumber = '';
}
if (empty($phoneNumber)){
$phoneNumber = '';
}
if (empty($creditCard)){
$creditCard = '';
}
if (empty($email)){
$email = '';
}

    //Create a new Customer object
    $Customer = new Customer($firstName, $lastName, $address, $phoneNumber, $creditCard, $email, $password, $passwordConfirmation, $userID, $member);
    $Customer->editCustomer();
    $Customer->closeDatabase();

?>
</p>
</div>
</div>
</div>
</div>
</div>

<div class="container container-footer">
    <!-- Footer -->
    <footer>
        <div class="row">
            <div class="col-lg-12 footer">
                <p><strong>&copy; Aloha Music Hall. LLC</strong></p
            </div>
        </div>
    </footer>

</div>
<!-- /.container -->

<!-- jQuery -->
<script src="js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

</body>

</html>


