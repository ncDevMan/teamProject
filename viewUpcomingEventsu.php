<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>View Upcoming Events</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/shop-item.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">Aloha Music Hall</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse toolbar" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <?php
//                    session_start();
                    if (isset($_SESSION["userID"])){
                        $sessionUserID = $_SESSION["userID"];
                        $link_address = 'login.php';
                        echo "<a href='".$link_address."'>".$sessionUserID."</a>";
                    }
                    else{
                        $link_address = 'login.php';
                        echo "<a href='".$link_address."'>Login</a>";
                    }
                    ?>
                </li>
                <li>
                    <a href="registerMember.php">Register New Customer</a>
                </li>
                <li>
                    <a href="viewUpcomingEventsu.php">View Upcoming Events</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!-- Page Content -->
<div class="container">
    <div class="row">
        <div class="col-md-3">
        </div>

        <div class="col-md-9 login-body-block body-block">
            <img src="img/theater-seats.jpg" class="viewIMG">
            <?php
            class Event{
                function Event(){
                    global $db;

                    //database local
                    $user = 'alohaadmin';
                    $pass = 'admin';
                    $host = 'localhost';
                    $database = 'alohamusichall';
                    try{
                        $db = new PDO('mysql:host='.$host.';dbname='.$database.'', $user, $pass);
                    }catch(PDOException $e){
                        print 'CANNOT CONNECT!'. $e->getMessage() . "<br/>";
                        die();
                    }
                }

                function eventBuilder(){
                    global $db;

                    $stmt = $db -> prepare("SELECT * from events");
                    $stmt -> execute();

                    $results = $stmt -> fetchAll();


                    return $results;


                }
            }


            //            Main-------------------------------------
            $Event = new Event();
            $results = $Event -> eventBuilder();

            ?>
            <div class ="login-reminder"><h2>You must be logged in to reserve a seat!</h2></div>
            <table class = "eventTable">

                <tr><th>Name</th> <th>Time</th> <th>Date</th> <th>Total Reserved</th> <th>Max Capacity</th> <th>Price</th></tr>
                <tr>

                    <td><?php echo $results['0']['name']; ?></td>
                    <td><?php echo $results['0']['time']; ?></td>
                    <td><?php echo $results['0']['date']; ?></td>
                    <td><?php echo $results['0']['totalReserved']; ?></td>
                    <td><?php echo $results['0']['maxCapacity']; ?></td>
                    <td><?php echo $results['0']['price']; ?></td>
                    <!--                    <td><button> Add </button></td>-->
                </tr>
                <tr>
                    <td><?php echo $results['1']['name']; ?></td>
                    <td><?php echo $results['1']['time']; ?></td>
                    <td><?php echo $results['1']['date']; ?></td>
                    <td><?php echo $results['1']['totalReserved']; ?></td>
                    <td><?php echo $results['1']['maxCapacity']; ?></td>
                    <td><?php echo $results['1']['price']; ?></td>
                </tr>
                <tr>
                    <td><?php echo $results['2']['name']; ?></td>
                    <td><?php echo $results['2']['time']; ?></td>
                    <td><?php echo $results['2']['date']; ?></td>
                    <td><?php echo $results['2']['totalReserved']; ?></td>
                    <td><?php echo $results['2']['maxCapacity']; ?></td>
                    <td><?php echo $results['2']['price']; ?></td>
                </tr>
                <tr>
                    <td><?php echo $results['3']['name']; ?></td>
                    <td><?php echo $results['3']['time']; ?></td>
                    <td><?php echo $results['3']['date']; ?></td>
                    <td><?php echo $results['3']['totalReserved']; ?></td>
                    <td><?php echo $results['3']['maxCapacity']; ?></td>
                    <td><?php echo $results['3']['price']; ?></td>
                </tr>
                <tr>
                    <td><?php echo $results['4']['name']; ?></td>
                    <td><?php echo $results['4']['time']; ?></td>
                    <td><?php echo $results['4']['date']; ?></td>
                    <td><?php echo $results['4']['totalReserved']; ?></td>
                    <td><?php echo $results['4']['maxCapacity']; ?></td>
                    <td><?php echo $results['4']['price']; ?></td>
                </tr>
            </table>
        </div>

    </div>

</div>

<div class="container container-footer">
    <!-- Footer -->
    <footer>
        <div class="row">
            <div class="col-lg-12 footer">
                <p><strong>&copy; Aloha Music Hall. LLC</strong></p>
            </div>
        </div>
    </footer>

</div>
<!-- /.container -->

<!-- jQuery -->
<script src="js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

</body>

</html>
