<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Reserve Seats!</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/shop-item.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="indexl.php">Aloha Music Hall</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse toolbar" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <!--                <li>-->
                <!--                    --><?php
                //                    session_start();
                //                    if (isset($_SESSION["userID"])){
                //                        $sessionUserID = $_SESSION["userID"];
                //                        $link_address = 'login.php';
                //                        echo "<a href='".$link_address."'>".$sessionUserID."</a>";
                //                    }
                //                    else{
                //                        $link_address = 'login.php';
                //                        echo "<a href='".$link_address."'>Login</a>";
                //                    }
                //                    ?>
                <!--                </li>-->
                <li>
                    <a href="index.php">Logout</a>
                </li>
                <li>
                    <a href="viewUpcomingEvents.php">View Upcoming Events</a>
                </li>
                <li>
                    <a href="reserveSeating.php">Reserve Seating</a>
                </li>
                <li>
                    <a href="cancelReservation.php">Cancel Reservations</a>
                </li>
                <li>
                    <a href="changePersonalInformation.php">Change Personal Information</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!-- Page Content -->
<div class="container">
    <div class="row">
        <div class="col-md-3">
        </div>

        <div class="col-md-9 login-body-block body-block">
            <img src="img/seatingChart.jpg" class="viewIMG">
            <?php
            class Event{

                /*
                 * event() Constructor
                 *
                 * @author: Nick
                 *
                 * Event Constructor- tests database for connection
                 *
                 * @return: void
                 */
                function Event(){
                    global $db;

                    //database local
                    $user = 'alohaadmin';
                    $pass = 'admin';
                    $host = 'localhost';
                    $database = 'alohamusichall';
                    //test database connection
                    try{
                        $db = new PDO('mysql:host='.$host.';dbname='.$database.'', $user, $pass);
                    }catch(PDOException $e){
                        print 'CANNOT CONNECT!'. $e->getMessage() . "<br/>";
                        die();
                    }
                }


                /*
                 * eventBuilder() Function
                 *
                 * @author: Nick
                 *
                 * This function will fetch all rows from the event database
                 * then store the information in an associative array.
                 *
                 * @return: results[] array
                 */
                function eventBuilder(){
                    global $db;

                    //SELECT ALL from events table
                    $stmt = $db -> prepare("SELECT * from events");
                    $stmt -> execute();

                    //fetch all rows from the database
                    $results = $stmt -> fetchAll();


                    return $results;


                }
            }


            //            Main-------------------------------------
            $Event = new Event();
            $results = $Event -> eventBuilder();



            ?>
            <div class="login-reminder"><h2>Reserve Seating</h2></div>
            <table class = "eventTable">

                <tr><th>EventID</th><th>Name</th> <th>Time</th> <th>Date</th> <th>Total Reserved</th> <th>Max Capacity</th> <th>Customer Price</th><th>Member Price</th></tr>
                <tr>
                    <td><?php echo $results['0']['id']; ?></td>
                    <td><?php echo $results['0']['name']; ?></td>
                    <td><?php echo $results['0']['time']; ?></td>
                    <td><?php echo $results['0']['date']; ?></td>
                    <td><?php echo $results['0']['totalReserved']; ?></td>
                    <td><?php echo $results['0']['maxCapacity']; ?></td>
                    <td><?php echo "$".$results['0']['price']; ?></td>
                    <td><?php $memberPrice = $results['0']['price'];
                        $priceOff = $memberPrice * 0.10;
                        echo "$".number_format($memberPrice-$priceOff, 2);
                        ?></td>
                </tr>
                <tr>
                    <td><?php echo $results['1']['id']; ?></td>
                    <td><?php echo $results['1']['name']; ?></td>
                    <td><?php echo $results['1']['time']; ?></td>
                    <td><?php echo $results['1']['date']; ?></td>
                    <td><?php echo $results['1']['totalReserved']; ?></td>
                    <td><?php echo $results['1']['maxCapacity']; ?></td>
                    <td><?php echo "$".$results['1']['price']; ?></td>
                    <td><?php $memberPrice = $results['1']['price'];
                        $priceOff = $memberPrice * 0.10;
                        echo "$".number_format($memberPrice-$priceOff, 2);
                        ?></td>
                </tr>
                <tr>
                    <td><?php echo $results['2']['id']; ?></td>
                    <td><?php echo $results['2']['name']; ?></td>
                    <td><?php echo $results['2']['time']; ?></td>
                    <td><?php echo $results['2']['date']; ?></td>
                    <td><?php echo $results['2']['totalReserved']; ?></td>
                    <td><?php echo $results['2']['maxCapacity']; ?></td>
                    <td><?php echo "$".$results['2']['price']; ?></td>
                    <td><?php $memberPrice = $results['2']['price'];
                        $priceOff = $memberPrice * 0.10;
                        echo "$".number_format($memberPrice-$priceOff, 2);
                        ?></td>
                </tr>
                <tr>
                    <td><?php echo $results['3']['id']; ?></td>
                    <td><?php echo $results['3']['name']; ?></td>
                    <td><?php echo $results['3']['time']; ?></td>
                    <td><?php echo $results['3']['date']; ?></td>
                    <td><?php echo $results['3']['totalReserved']; ?></td>
                    <td><?php echo $results['3']['maxCapacity']; ?></td>
                    <td><?php echo "$".$results['3']['price']; ?></td>
                    <td><?php $memberPrice = $results['3']['price'];
                        $priceOff = $memberPrice * 0.10;
                        echo "$".number_format($memberPrice-$priceOff, 2);
                        ?></td>
                </tr>
                <tr>
                    <td><?php echo $results['4']['id']; ?></td>
                    <td><?php echo $results['4']['name']; ?></td>
                    <td><?php echo $results['4']['time']; ?></td>
                    <td><?php echo $results['4']['date']; ?></td>
                    <td><?php echo $results['4']['totalReserved']; ?></td>
                    <td><?php echo $results['4']['maxCapacity']; ?></td>
                    <td><?php echo "$".$results['4']['price']; ?></td>
                    <td><?php $memberPrice = $results['4']['price'];
                        $priceOff = $memberPrice * 0.10;
                        echo "$".number_format($memberPrice-$priceOff, 2);
                        ?></td>
                </tr>
            </table>
            <div class="member-reminder"><strong>**Member Pricing will be calculated at checkout**</strong></div>

            <p>
            <div class="main-form">
            <form name="form" action="processSeating.php" name="form" method="post"><strong>
                    EventID:<br/>
                    <input type="text" name="eventid">
                </strong>
                <br/>
                <br/>
                <strong>
                    Quantity<br/>
                    <input type="text" name="quantity">
                </strong>
                <br/>
                ***You are allowed <strong>4</strong> reservations per account!***
                <br/>
                <br/>
                <br/>
                <br/>


                <select name='priority'>
                    <option>Seat Priority</option>
                    <option value="GA">General Admission</option>
                    <option value="VIP">VIP</option>
                </select>


                <br/>
                The VIP Section is the first <strong>4</strong> rows in the theater. Anything else is <strong>General Admission</strong>.
                <br/>
                <br/>
                <h2>For verification, Please enter your User ID and password</h2>
                <strong>
                    UserID: <br/>
                    <input type ='text' name = 'userid'>
                </strong>
                <br/>
                <br/>
                <strong>
                    Login Password:<br/>
                    <input type="password" name="password">
                </strong>
                <br/>
                <br/>
                <strong>
                    Login Password Confirmation:<br/>
                    <input type="password" name="passwordconfirm">
                </strong>
                <br/>
                <br/>
                <strong>Pressing submit will start the transaction process. Your card attached to this account will be billed!</strong>
                <br/>
                <input type="submit" onclick="return validateForm()"/>
                </form>
                </p>
            </div>
        </div>

    </div>

</div>
<div class="container container-footer">
    <!-- Footer -->
    <footer>
        <div class="row">
            <div class="col-lg-12 footer">
                <p><strong>&copy; Aloha Music Hall. LLC</strong></p>
            </div>
        </div>
    </footer>

</div>
<!-- /.container -->

<!-- jQuery -->
<script src="js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

<script src="js/validation.js"></script>

</body>

</html>
