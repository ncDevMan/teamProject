<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Reserve Seating</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/shop-item.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="indexl.php">Aloha Music Hall</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse toolbar" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="index.php">Logout</a>
                </li>
                <li>
                    <a href="viewUpcomingEvents.php">View Upcoming Events</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!-- Page Content -->
<div class="container">
    <div class="row">
        <div class="col-md-3">
        </div>

        <div class="col-md-9 login-body-block body-block">
            <div class="thumbnail">
                <div class="caption-full main-form">
                    <?php

                    class Event{

                        /*
                         * event() Constructor
                         *
                         * @author: Nick
                         *
                         * Event Constructor- tests database for connection
                         *
                         * @return: void
                         */
                        function Event(){
                            global $db;

                            //database local
                            $user = 'alohaadmin';
                            $pass = 'admin';
                            $host = 'localhost';
                            $database = 'alohamusichall';

                            //test database connection
                            try{
                                $db = new PDO('mysql:host='.$host.';dbname='.$database.'', $user, $pass);
                            }catch(PDOException $e){
                                print 'CANNOT CONNECT!'. $e->getMessage() . "<br/>";
                                die();
                            }
                        }


                        /*
                         * eventBuilder() Function
                         *
                         * @author: Nick
                         *
                         * This function will fetch all rows from the event database
                         * then store the information in an associative array.
                         *
                         * @return: results[] array
                         */
                        function eventBuilder(){
                            global $db;

                            //SELECT ALL from events table
                            $stmt = $db -> prepare("SELECT * from events");
                            $stmt -> execute();

                            //fetch all rows from the database
                            $results = $stmt -> fetchAll();


                            return $results;


                        }

                        /*
                         * updateSeats() function
                         * @author: Nick
                         *
                         * This function will update the total number of seats in the database- depending on eventID.
                         *
                         * @return: void
                         */
                        function updateSeats($eventID, $quantity){
                            global $db;

                            $sql = "INSERT INTO events(totalReserved) WHERE id = :eventID VALUES(SUM(totalReserved,:quantity))";
                            $q = $db -> prepare($sql);
                            $q->execute(array(':eventID' => $eventID,
                                ':quantity' => $quantity
                            ));
                        }
                        /*
                         * updateVIP() function
                         * @author: Nick
                         *
                         * This function will update the total number of VIP seats in the database- depending on eventID.
                         *
                         * @return: void
                         */
                        function updateVIP($eventID, $quantity){
                            global $db;

                            $sql = "INSERT INTO events(vipReserved) WHERE id = :eventID VALUES(SUM(vipReserved,:quantity))";
                            $q = $db -> prepare($sql);
                            $q->execute(array(':eventID' => $eventID,
                                ':quantity' => $quantity
                            ));
                        }

                    }


                    class Customer{
                        function Customer(){
                            global $db;

                            //database local
                            $user = 'alohaadmin';
                            $pass = 'admin';
                            $host = 'localhost';
                            $database = 'alohamusichall';

                            //test database connection
                            try{
                                $db = new PDO('mysql:host='.$host.';dbname='.$database.'', $user, $pass);
                            }catch(PDOException $e){
                                print 'CANNOT CONNECT!'. $e->getMessage() . "<br/>";
                                die();
                            }
                        }


                        /*
                         * checkLogin() function
                         *
                         * @author: Nick
                         *
                         * This function will check the login credentials for the system and will output a page accordingly.
                         *
                         * @return: void
                         */
                        function checkLogin($userID, $password){

                            global $db;

                            $stmt = ("SELECT userid, password FROM customers WHERE userID = :userid AND password = :password");
                            $sql = $db -> prepare ($stmt);
                            $sql-> execute(array(':userid'=>$userID, ':password' => $password));

                            //Fetch Boolean
                            $results = $sql->fetch(PDO::FETCH_ASSOC);

                            //If the user name and password does not exist
                            if(!$results){
                                return false;

                            }

                            //The user name and password exists!!!!!
                            else{
                                return true;
                            }

                        }

                        /*
                         * checkReservations() Function
                         * @author: Nick
                         *
                         * This function will check if the user has any reservations availble.
                         *
                         * @return array
                         */
                       function checkReservations($userID, $password){
                           global $db;
                           //Check if the customer has any reservations availble
                           $stmt = ("SELECT reservationsTotal FROM customers WHERE userID = :userid AND password = :password");
                           $sql = $db -> prepare ($stmt);
                           $sql-> execute(array(':userid'=>$userID, ':password' => $password));
                           $resevationResults = $sql->fetch(PDO::FETCH_NUM);

                           return $resevationResults;
                       }

                        /*
                         * getInformation function()
                         * @author: Nick
                         *
                         * This function will grab all of the Customers information- depnding on the USERid
                         *
                         * @RETURN: resultsArray[]
                         *
                         */
                        function getInformation($userID){
                            global $db;

                            $stmt = $db -> prepare("SELECT * from customers WHERE userID = :userID");
                            $stmt -> execute(array(':userID' => $userID));

                            $results = $stmt -> fetchAll(PDO::FETCH_ASSOC);

                            return $results;

                        }

                        /*
                         * calculateTotal() function
                         * @author: Nick
                         *
                         * This function is used to calculate the total. Members and customers
                         * have different prices and you are allowed up to 4 reservations.
                         *
                         * @return int
                         */
                        function calculateTotal($quantity, $price, $member){
                            if ($member == 1){
                                $priceOff = $price * 0.10;
//                                echo "$".number_format($memberPrice-$priceOff, 2);
                                $price = ($price - $priceOff) * $quantity;

                                $price = number_format($price, 2);
                                return $price;
                            }

                            //Customer rate
                            else{
                                $price = $price * $quantity;

                                $price = number_format($price, 2);
                                return $price;

                            }


                        }


                        /*
                         * grabEventInfo() function
                         * @author: Nick
                         *
                         * This function will grab all the event info from the database.
                         * This is used to show the events table.
                         *
                         * @return resultsArray[]
                         */
                        function grabEventInfo($eventID){
                            global $db;

                            //SELECT ALL from events table
                            $stmt = $db -> prepare("SELECT name, price from events WHERE id = :eventid");
                            $stmt -> execute(array(
                                ':eventid' => $eventID
                            ));

                            //fetch all rows from the database
                            $eventResults = $stmt -> fetchAll(PDO::FETCH_ASSOC);

                            return $eventResults;
                        }

                        /*
                         * getConfirmation() function
                         * @author: Nick
                         *
                         * This function will create a confirmation number.
                         *
                         * @return: int
                         */
                        function getConfirmation(){
                            $number = rand(0,10000);

                            return $number;

                        }


                        /*
                         * recordConfirmation() function
                         * @author: Nick
                         *
                         * this function will add the confirmation number to the Customers profile in the database.
                         * This confirmation number is given when you reserve a seat(s)
                         */
                        function recordConfirmation($number, $userID){
                            global $db;

                            $sql = "INSERT INTO customers(confirmNumber) WHERE userID = :userID VALUES(:number)";
                            $q = $db -> prepare($sql);
                            $q->execute(array(':userID' => $userID,
                            ':number' => $number
                            ));

                        }

                        /*
                         * updateReservations() function
                         * @author: Nick
                         *
                         * This function will update the CUSTOMERS reservations total in the database;
                         *
                         * @return: void
                         */
                        function updateReservations($userID, $quantity){
                                global $db;

                                $sql = "INSERT INTO customers(reservationsTotal) WHERE userID = :userID VALUES(SUM(reservationsTotal,:quantity))";
                                $q = $db -> prepare($sql);
                                $q->execute(array(':userID' => $userID,
                                    ':quantity' => $quantity
                                ));

                        }


                    }
                    $userID = $_POST['userid'];
                    $password = $_POST['password'];
                    $eventID = $_POST['eventid'];
                    $quantity = $_POST['quantity'];
                    $seatPriority = $_POST['priority'][0];

                    //                    Main---------------------------------------------------------------------------------------------

                    $Customer = new Customer();
                    //username and password exists and are validated
                    if($Customer->checkLogin($userID, $password)){

                        $reservationResults = $Customer->checkReservations($userID, $password);

                        //Maximum number of reservations
                        if ($reservationResults == 4){
                            echo "<h2>Error</h2>";
                            echo "<br/>";
                            echo "<p>";

                            $link_address = 'cancelReservation.php';
                            print "<h3>You have the maximum number of reservations!</h3>";
                            echo "<br/>";
                            print "Please go back" ."<a href='$link_address'><button>Back</button></a>". " and try again.";
                        }
                        //Less than 4 reservations!!!
                        else{
                            $results = $Customer->getInformation($userID);
                            $eventResults = $Customer -> grabEventInfo($eventID);

                            $firstName = $results['0']['firstName'];
                            $lastName = $results['0']['lastName'];
                            $address = $results['0']['address'];
                            $phoneNumber = $results['0']['phoneNumber'];
                            $creditCard = $results['0']['creditCard'];
                            $email = $results['0']['email'];
                            $reservationsTotal = $results['0']['reservationsTotal'];
                            $member = $results['0']['isMember'];


                            $status = '';
                            if ($results['0']['isMember'] == 1){
                                $status = "Member";
                            }
                            else{
                                $status = 'Customer';
                            }

                            $seatStatus = '';
                            //check if VIP or GA seat
                            if ($seatPriority == 'V'){
                                $seatStatus = "V.I.P.";
                            }
                            elseif($seatPriority == 'G'){
                                $seatStatus = "General Admission";
                            }

                            //Grab the event name from the database
                            $eventName = $eventResults['0']['name'];

                            //Grab the event price from the database
                            $eventPrice = $eventResults['0']['price'];

                            //Get Total for Member
                            $total = $Customer -> calculateTotal($quantity, $eventPrice, $member);

                            $confirmationNumber = $Customer -> getConfirmation();


                            echo "<h2>All done ".$firstName. ' '.$lastName."!</h2>";
                            echo "<strong>You have registered ".$quantity."".' '.$seatStatus." seats to see ". $eventName ."!</strong>";
                            echo "<br/>";
                            echo "Your card on record has been billed <strong>$".$total."</strong> dollars.";
                            echo "<br/>";
                            echo "<br/>";
                            echo "<br/>";
                            echo "Your event confirmation number is: <h3>".$confirmationNumber."</h3> save this- as this is useful information.";


                            $Customer->recordConfirmation($confirmationNumber, $userID);

                            $Event = new Event();

                            $Event -> updateSeats($eventID, $quantity);
                            //Update seat count in events table to +1

                            if ($seatPriority == 'V'){
                                $Event -> updateVIP($eventID, $quantity);
                            }

                            $Customer -> updateReservations($userID, $quantity);
                            //Update reservations
                        }



                    }
                    else{
                        echo "<h2>Error</h2>";
                        echo "<br/>";
                        echo "<p>";

                        $link_address = 'reserveSeating.php';
                        print "<h3>You have invalid login credentials!</h3>";
                        echo "<br/>";
                        print "Please go back" ."<a href='$link_address'><button>Back</button></a>". " and try again.";
                    }
                    ?>
                </div>
            </div>

        </div>

    </div>

</div>

<div class="container container-footer process-login-footer">
    <!-- Footer -->
    <footer>
        <div class="row">
            <div class="col-lg-12 footer">
                <p><strong>&copy; Aloha Music Hall. LLC</strong></p>
            </div>
        </div>
    </footer>

</div>
<!-- /.container -->

<!-- jQuery -->
<script src="js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

</body>

</html>
