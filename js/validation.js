/**
 * Created by Nick on 5/8/15.
 */


//function cardnumber(inputtxt)
//{
//    var cardno = /^(?:4[0-9]{12}(?:[0-9]{3})?)$/;
//    if(inputtxt.value.match(cardno))
//    {
//        return true;
//    }
//    else
//    {
//
//        return false;
//    }
//}

function validateForm(){
    var fname = document.forms["form"]['firstname'].value;
    if (fname == null || fname == "") {
        alert("Name must be filled out");
        return false;
    }


    var lname = document.forms["form"]['lastname'].value;
    if (lname == null || lname == "") {
        alert("Name must be filled out");
        return false;
    }


    var address = document.forms["form"]['address'].value;
    if (address == null || address == "") {
        alert("Address must be filled out");
        return false;
    }

    var phonenumber = document.forms["form"]['phonenumber'].value;
    if (phonenumber == null || phonenumber == "") {
        alert("Phone Number must be filled out");
        return false;
    }

    var creditcard = document.forms["form"]['creditcard'].value;
    if (creditcard == null || creditcard == "") {
        alert("CreditCard must be filled out");
        return false;
    }

    var email = document.forms["form"]['email'].value;
    if (email == null || email == "" && validateEmail(email)) {
        alert("Email must be filled out");
        return false;
    }

    var password = document.forms["form"]['password'].value;
    if (password == null || password == "") {
        alert("password must be filled out");
        return false;
    }

    var passwordconfirm = document.forms["form"]['passwordconfirm'].value;
    if (passwordconfirm == null || passwordconfirm == "") {
        alert("passwordconfirm must be filled out");
        return false;
    }

    var userid = document.forms["form"]['userid'].value;
    if (userid == null || userid == "") {
        alert("userid must be filled out");
        return false;
    }

    var eventid = document.forms["form"]['eventid'].value;
    if (eventid == null || eventid == "") {
        alert("EVENTID must be filled out");
        return false;
    }



}